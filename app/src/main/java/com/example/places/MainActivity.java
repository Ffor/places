package com.example.places;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.foursquare.api.types.Venue;
import com.foursquare.api.types.geofence.GeofenceEvent;
import com.foursquare.pilgrim.CurrentLocation;
import com.foursquare.pilgrim.LogLevel;
import com.foursquare.pilgrim.PilgrimNotificationHandler;
import com.foursquare.pilgrim.PilgrimSdk;
import com.foursquare.pilgrim.PilgrimSdkBackfillNotification;
import com.foursquare.pilgrim.PilgrimSdkGeofenceEventNotification;
import com.foursquare.pilgrim.PilgrimSdkVisitNotification;
import com.foursquare.pilgrim.Result;
import com.foursquare.pilgrim.Visit;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final int REQ_LOCATION = 1;

    private final PilgrimNotificationHandler pilgrimNotificationHandler = new PilgrimNotificationHandler() {
        // Primary visit handler
        @Override
        public void handleVisit(Context context, PilgrimSdkVisitNotification notification) {
            // Process the visit however you'd like:
            Visit visit = notification.getVisit();
            Venue venue = visit.getVenue();
            Log.d("test", visit.toString());
        }

        // Optional: If visit occurred while in Doze mode or without network connectivity
        @Override
        public void handleBackfillVisit(Context context, PilgrimSdkBackfillNotification notification) {
            // Process the visit however you'd like:
            super.handleBackfillVisit(context, notification);
            Visit visit = notification.getVisit();
            Venue venue = visit.getVenue();
            Log.d("test", visit.toString());

        }

        // Optional: If visit occurred by triggering a geofence
        @Override
        public void handleGeofenceEventNotification(Context context, PilgrimSdkGeofenceEventNotification notification) {
            // Process the geofence events however you'd like:
            List<GeofenceEvent> geofenceEvents = notification.getGeofenceEvents();
            for (GeofenceEvent geofenceEvent : geofenceEvents) {
                Log.d("test", geofenceEvent.toString());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        PilgrimSdk.Builder builder = new PilgrimSdk.Builder(this)
                .consumer("ER40J1Z0JSU4DI0DBLITWF2PPNBXTNSQQUJZYHMSW5DFSAN0", "4ARR3NDZSXNP3XHKQKEOR2JJPLX5CBI1D42PFMI3FTMFIEAC")
                .notificationHandler(pilgrimNotificationHandler)
                .logLevel(LogLevel.DEBUG);
        PilgrimSdk.with(builder);

        PilgrimSdk.start(this);

        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQ_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "PERMISSION GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void getCurrentLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Result<CurrentLocation, Exception> result = PilgrimSdk.get().getCurrentLocation();
                    if (result.isOk()) {
                        CurrentLocation currentLocation = result.getOrNull();
                        Log.d("test", "Currently at " + currentLocation.getCurrentPlace().toString() + " and inside " + result.getOrNull().getMatchedGeofences().size() + " geofence(s)");
                    } else {
                        Log.e("test", result.getErr().getMessage(), result.getErr());
                    }
                }
            }).start();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQ_LOCATION);
        }
    }
}
